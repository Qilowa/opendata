<?php

class Etablissement {
    private $code;
    private $nom;
    private $longitude;
    private $latitude;
    private $url;
    private $ville;
    private $wiki;
    private $adresse;
    private $type;

    public function __construct($code, $nom, $longitude, $latitude, $url, $ville, $wiki, $adresse, $type) {
        $this->code = $code;
        $this->nom = $nom;
        $this->longitude = $longitude;
        $this->latitude = $latitude;
        $this->url = $url;
        $this->ville = $ville;
        $this->wiki = $wiki;
        $this->adresse = $adresse;
        $this->type = $type;
    }

    public function getName() {
      return $this->nom;
    }

    public function getCode() {
      return $this->code;
    }

    public function getCoord() {
      return array($this->longitude, $this->latitude);
    }

    public function getUrl() {
      return $this->url;
    }

    public function getVille() {
      return $this->ville;
    }

    public function toString() {
      echo "<table>
        <tr>
          <th>Nom de l'établissement</th>
          <td>".$this->nom."</td>
        </tr>
        <tr>
          <th>Adresse</th>
          <td>".$this->type."</td>
        </tr>
        <tr>
          <th>Site web de l'établissement</th>
          <td><a href='".$this->url."'>".$this->url."</td>

        </tr>
        <tr>
          <th>Ville de l'établissement</th>
          <td>".$this->ville."</td>
        </tr>
        <tr>
          <th>Wikipedia</th>
          <td><a href='".$this->wiki."'>".$this->wiki."</td>
        </tr>
        <tr>
          <th>Adresse</th>
          <td>".$this->adresse."</td>
        </tr>

      </table>";
    }


}




 ?>
