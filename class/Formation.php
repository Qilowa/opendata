<?php
class Formation {
  private $nomEtab;
  private $secteur;
  private $type;
  private $niveau;
  private $effectif;
  private $diplom;

  public function __construct($nomEtab, $secteur,  $niveau, $type, $effectif, $diplom) {
    $this->nomEtab = $nomEtab;
    $this->secteur = $secteur;
    $this->type = $type;
    $this->niveau = $niveau;
    $this->effectif = $effectif;
    $this->diplom = $diplom;
  }

  public function getNomEtab() {
    return $this->nomEtab;
  }

  public function getSecteur() {
    return $this->secteur;
  }

  public function getType() {
    return $this->type;
  }

  public function getNiveau() {
    return $this->niveau;
  }

  public function getEffectif() {
    return $this->effectif;
  }

  public function getCodeDiplom() {
    return $this->diplom;
  }

  public function toString() {
    echo "<table>
      <tr>
        <th>Nom de la formation</th>
        <td>".$this->nom."</td>
      </tr>
      <tr>
        <th>Secteur</th>
        <td>".$this->secteur."</td>
      </tr>
      <tr>
        <th>Niveau de la formation</th>
        <td><a href=".$this->niveau."</td>

      </tr>
      <tr>
        <th>Type de formation</th>
        <td>".$this->type."</td>
      </tr>
      <tr>
        <th>Effectif</th>
        <td>".$this->effectif."</td>
      </tr>


    </table>";
  }


}




 ?>
