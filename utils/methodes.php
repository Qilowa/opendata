<?php


function sort_facets($results) {
  $array = array();

  // Ajout des domaines dans un array vide
  foreach($results["facet_groups"][0]["facets"] as $key => $value) {
    $formation = $value["name"];
    array_push($array, $formation);
  }
  asort($array);
  return $array;
}

function printOptions($array) {
  foreach($array as $value) {
      printf('<option value="%s">%s</option>', $value, $value);
  }
}

function printArray($array) {
  // $array -> une dimension
  echo "<tr>\n";
  foreach($array as $value) {
    echo "<td>".$value."</td>\n";
  }
  echo "</tr>\n";
}

function createAllCount($path) {
  // Créé un tableau avec comme clef le code d'un établissement/formation et comme valeur le nombre de visiteur
  $data = file_get_contents($path);
  $lines = explode("\n", $data);
  $array = array();
  foreach($lines as $value) {
    $l = explode(";", $value);
    if (isset($l[1]) && $l[1] != '') {
      $etab = $l[0];
      $count = $l[1];
      $array[$etab] = $count;
    }
  }
  return $array;
}

function addNewElement($array, $element, $link) {
  // Ajoute un visiteur dans le fichier correspondant à $link
  if (!isset($array[$element])) {
    $array[$element] = 0;
  }

  $array[$element] += 1;
  $complete = getcwd()."/".$link;

  $handle = fopen($complete, "w");

  foreach($array as $key=>$value) {
    $f = fwrite($handle, $key.";".$value."\n");
  }
  fclose($handle);
}

function getMostVisited($path) {
  // A finir;
  $data = createAllCount($path);

  $array = array();
  for($i=0; $i<3; $i++) {
    $maxVisited = 0;
    $code = "";
    foreach($data as $key=>$nb) {
      if ($nb >= $maxVisited) {
        $maxVisited = $nb;
        $code = $key;
      }
    }
    $array[$code] = $maxVisited;
    unset($data[$code]);
  }
  return $array;

}

 ?>
