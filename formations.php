<!DOCTYPE html>
<?php
require("class/API.php");
require("utils/methodes.php");
?>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <link href="style.css" rel="stylesheet">
    <link href="style2.css" rel="stylesheet">
    <title>PostBac</title>
  </head>

  <body>
    <nav>
      <ul>
        <li><a href="index.html" class="nav navlogo"><img src="files/logo.png" class="logo" alt="Logo">
           PostBac
        </a>
      </li>
        <li><a href="formations.php" class="nav formation">Formations</a></li>
        <li><a href="region.php" class="nav region">Régions</a></li>
        <li><a href="bestFormation.php" class="nav information">Les + visités</a></li>
        <li><a href="informations.html" class="nav information">Informations</a></li>
      </ul>
    </nav>

    <div class="contenu fond2">
      <br>
      <div class="container research">
      <h1>Rechercher une formation</h1>
      <br>
      <div class="search">
        <h2>Entrez un mot-clef</h2>
        <form name="form" action="resultat.php" method="post">
          <img src="files/loupe.svg" class="gloupe" alt="Image loupe">
          <input type="text" name="recherche">
        </form>
      </div>
      <br>
      <br>
      <h2>Par critère</h2>
      <div class="criteres">
        <form action="resultat1.php" name="criteres" method="post">
        <div class="form">
          <label for="domain">Domaine : </label>
          <select name="domain" id="domain">
            <option value="" selected="selected" disabled="disabled">Domaines</option>
            <?php


              $url = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=0&sort=-rentree_lib&facet=discipline_lib";
              $results = API::getInformations($url);
              $array = sort_facets($results);
              printOptions($array);

             ?>
          </select>
      </div>
      <br>
      <div class="form">
        <label for="departement">Département : </label>
        <select name="departement" id="departement">
          <option value="" selected="selected" disabled="disabled">Département</option>
          <?php

            $url = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=0&sort=-rentree_lib&facet=dep_etab_lib";
            $results = API::getInformations($url);

            $array = sort_facets($results);
            printOptions($array);
           ?>
        </select>
      </div>
      <br>
      <div class="form">
        <label for="diplome">Diplome</label>
        <select name="diplome" id="diplome">
          <option value="" selected="selected" disabled="disabled">Diplomes</option>
          <?php
            $url = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=0&sort=-rentree_lib&facet=diplome_lib";
            $results = API::getInformations($url);

            $array = sort_facets($results);
            printOptions($array);
          ?>
          </select>
      </div>

      <br>
      <div class="form">
        <label for="niveau">Niveau d'étude : </label>
        <select name="niveau" id="niveau">
          <option value="" selected="selected" disabled="disabled">Niveau d'étude</option>
          <?php
            $url = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=0&facet=niveau_lib";
            $results = API::getInformations($url);

            $array = sort_facets($results);
            printOptions($array);
           ?>
        </select>
      </div>
      <br>
      <br>
      <h3>Domaine plus précis ? </h3>
      <div class="form">
        <label for="preciseDomain">Domaine précis : </label>
        <select name="preciseDomain" id="preciseDomain">
          <option value="" selected="selected" disabled="disabled">Domaine précis</option>
          <?php
            $url = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=0&facet=sect_disciplinaire_lib";
            $results = API::getInformations($url);

            $array = sort_facets($results);
            printOptions($array);
           ?>
        </select>
      </div>

    <br>
    <br>
    <button class="btn btn-1 btn-sep icon-info" formmethod="post">Rechercher</button>
  </form>
      </div>
  </div>
  </div>
  <footer>
    <p>Le repo : <a href="https://bitbucket.org/Qilowa/opendata/src/master/">https://bitbucket.org/Qilowa/opendata/src/master/</a></p>
  </footer>
  </body>

</html>
