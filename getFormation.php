<!DOCTYPE html>
<?php
require('class/API.php');
require("class/Formation.php");
require("class/Etablissement.php");
require("utils/methodes.php");
?>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <link href="style.css" rel="stylesheet">
    <title>Post Bac</title>
  </head>

  <body>
    <nav>
      <ul>
        <li><a href="index.html" class="nav navlogo"><img src="files/logo.png" class="logo" alt="Logo">
           PostBac
        </a>
      </li>
        <li><a href="formations.php" class="nav formation">Formations</a></li>
        <li><a href="region.php" class="nav region">Régions</a></li>
        <li><a href="bestFormation.php" class="nav information">Les + visités</a></li>
        <li><a href="informations.html" class="nav information">Informations</a></li>
      </ul>
    </nav>
    <div class="contenu">
      <br>
      <div class="container">
        <div class="test">
        <?php
        if (isset($_GET)) {
            // print_r($_GET);

            //Compter le nombre de visiteur

            //On forme l'array
            $array = createAllCount("utils/counts/count_form.txt");
            // On ajoute 1 visiteur
            addNewElement($array, $_SERVER["REQUEST_URI"], "utils/counts/count_form.txt");

            // On affiche la valeur qui est à jour
            $array = createAllCount("utils/counts/count_form.txt");

            ?>
        <div class="left">
          <h1><?php echo $_GET["diplome"]." ".$_GET["sect"]." à ".$_GET["etab"] ?></h1>
          <br>
          <p>Nombre de visite : <?php echo $array[$_SERVER["REQUEST_URI"]] ?></p>
          <table>
            <tr>
              <th>Nom de l'établissement</th>
              <td><?php echo $_GET["etab"] ?></td>
            </tr>
            <tr>
              <th>Secteur</th>
              <td><?php echo $_GET["sect"] ?></td>
            </tr>
            <tr>
              <th>Intitulé</th>
              <td><?php echo $_GET["lib"] ?></td>
            </tr>
            <tr>
              <th>Type de diplome</th>
              <td><?php echo $_GET["diplome"] ?></td>
            </tr>
            <tr>
              <th>Niveau de la formation</th>
              <td><?php echo $_GET["niv"] ?></td>
            </tr>
            <tr>
              <th>Localisation de l'établissement</th>
              <td><?php echo $_GET["loc"] ?></td>
            </tr>
            <tr>
              <th>Ville de l'établissement</th>
              <td><?php echo $_GET["ville"] ?></td>
            </tr>
            <tr>
              <th>Effectif</th>
              <td><?php echo $_GET["effectif"] ?></td>
            </tr>
            <tr>
              <th>Wikipedia</th>
              <td><a href=<?php echo $_GET["wiki"]; ?>><?php echo $_GET["wiki"] ?></a></td>
            </tr>
            <tr>
              <th>Site web de l'établissement</th>
              <td><a href=<?php echo $_GET["site"]; ?>><?php echo $_GET["site"] ?></a></td>
            </tr>
          </table>
        </div>

        <?php }
        ?>
      </div>
    </div>
    </div>
    <footer>
      <p>Le repo : <a href="https://bitbucket.org/Qilowa/opendata/src/master/">https://bitbucket.org/Qilowa/opendata/src/master/</a></p>
    </footer>
  </body>

</html>
