<!DOCTYPE html>
<?php
  require("utils/methodes.php");
  require("class/API.php");
 ?>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <link href="style.css" rel="stylesheet">
    <link href="style2.css" rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
  integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
  crossorigin=""/>

  <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
  integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
  crossorigin=""></script>
    <title>PostBac</title>
  </head>

  <body>
    <nav>
      <ul>
        <li><a href="index.html" class="nav navlogo"><img src="files/logo.png" class="logo" alt="Logo">
           PostBac
        </a>
      </li>
        <li><a href="formations.php" class="nav formation">Formations</a></li>
        <li><a href="region.php" class="nav region">Régions</a></li>
        <li><a href="bestFormation.php" class="nav information">Les + visités</a></li>
        <li><a href="informations.html" class="nav information">Informations</a></li>

      </ul>
    </nav>
    <div class="contenu fond3">
      <br>
      <br>

      <div class="text">
        <div class="etab">
        <h1>Les établissements les plus visités</h1>
        <ul>
        <?php
          $etabs = getMostVisited("utils/counts/count_etab.txt");

          $count = 1;

          foreach($etabs as $key=>$value) {
            $url_getuai = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-etablissements-enseignement-superieur&facet=uai&facet=type_d_etablissement&facet=com_nom&facet=dep_nom&facet=aca_nom&facet=reg_nom&facet=pays_etranger_acheminement";
            $url_getuai .= "&refine.uai=".$key;
            $res = API::getInformations($url_getuai);

            $nom = $res["records"][0]["fields"]["uo_lib"];
            ?>
            <li class="medal<?php echo $count ?>">
            <div class="wrapper">

              <span class="square">

                <a class="ninth before after" href="getEtablissement.php?id=<?php echo $key ?>"><?php echo $nom ?></a>

              </span>

            </div>
            </li>
          <?php
            $count+=1;
        }

         ?>
       </ul>
     </div>
       <br>
       <div class="formas">
       <h1>Les formations les plus visités</h1>
       <ul>
       <?php
         $etabs = getMostVisited("utils/counts/count_form.txt");

         $count = 1;

         foreach($etabs as $key=>$value) {
            $l = explode("diplome=", $key);
            if (isset($l[1])) {
            $diplome = explode("&", $l[1])[0];

            $sect = urldecode(explode("sect=", $l[0])[1]);
            $sect1 = explode("&", $sect)[0];

            $etab = urldecode(explode("etab=", $l[0])[1]);
            $etab1 = explode("&", $etab)[0];

            $name = $diplome." ".$sect1." à ".$etab1;

           ?>
           <li class="medal<?php echo $count ?>">
           <div class="wrapper">

             <span class="square">

               <a class="ninth before after" href="<?php echo $key ?>"><?php echo $name ?></a>


             </span>

           </div>
           </li>
         <?php
          }
           $count+=1;
       }

        ?>
      </ul>
    </div>
      </div>


    </div>
    <footer>
      <p>Le repo : <a href="https://bitbucket.org/Qilowa/opendata/src/master/">https://bitbucket.org/Qilowa/opendata/src/master/</a>
      <br>Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a></p>
    </footer>
  </body>

</html>
