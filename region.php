<!DOCTYPE html>
<?php
require("class/API.php");
 ?>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <link href="style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
  integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
  crossorigin=""/>

  <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
  integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
  crossorigin=""></script>
    <title>PostBac</title>
  </head>

  <body>
    <nav>
      <ul>
        <li><a href="index.html" class="nav navlogo"><img src="files/logo.png" class="logo" alt="Logo">
           PostBac
        </a>
      </li>
        <li><a href="formations.php" class="nav formation">Formations</a></li>
        <li><a href="region.php" class="nav region">Régions</a></li>
        <li><a href="bestFormation.php" class="nav information">Les + visités</a></li>
        <li><a href="informations.html" class="nav information">Informations</a></li>
      </ul>
    </nav>
    <div class="contenu">
      <br>
      <div class="container map">
      <h1>Formation par région</h1>

      <div class="test">
       <div id="mapid"></div>
       <script>
         var mymap = L.map('mapid').setView([48.8385102, 2.5883073], 16);
         L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 16,
        id: 'mapbox/streets-v11',
        accessToken: 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw'
        }).addTo(mymap);

        L.marker([48.8385102, 2.5883073]).bindPopup("<b>Université Paris-Est Marne-la-Vallée<b>").openPopup();

        function onLocationFound(e) {
        		var radius = e.accuracy / 2;

        		L.marker(e.latlng).addTo(mymap)
        			.bindPopup("You are within " + radius + " meters from this point").openPopup();

            console.log(e.latlng);

        		L.circle(e.latlng, radius).addTo(mymap);

            var longitude = e.latlng["lng"];
            var latitude = e.latlng["lat"];
            var range = 1;
            <?php
              $url = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-etablissements-enseignement-superieur&facet=uai&facet=type_d_etablissement&facet=com_nom&facet=dep_nom&facet=aca_nom&facet=reg_nom&facet=pays_etranger_acheminement&rows=322";
              $results = API::getInformations();

              foreach($results["records"] as $key => $value) {
                $coord1 = $value["fields"]["coordonnees"][0];
                $coord2 = $value["fields"]["coordonnees"][1]; ?>
                <?php $type = $value["fields"]["type_d_etablissement"];

                if (isset($coord1) && isset($coord2)) { ?>

                    var marker = L.marker([<?php echo $coord1; ?>, <?php echo $coord2; ?>]).addTo(mymap).bindPopup("<?php echo $type ?><br>Site web : <a href='<?php echo $site?>'><?php echo $site?></a>").openPopup();


                <?php }

                ?>

                <?php
              }

              ?>
        	}

        	function onLocationError(e) {
        		alert(e.message);
        	}

        	mymap.on('locationfound', onLocationFound);
        	mymap.on('locationerror', onLocationError);

        	mymap.locate({setView: true, maxZoom: 16});

        </script>
        <!-- <div class="rech">
          <img src="files/loupe.svg" class="gloupe2">
          <input type="text" class="small">
        </div> -->

        </div>
    </div>
    </div>
    <footer>
      <p>Le repo : <a href="https://bitbucket.org/Qilowa/opendata/src/master/">https://bitbucket.org/Qilowa/opendata/src/master/</a></p>
    </footer>
  </body>

</html>
