<!DOCTYPE html>
<?php
require('class/API.php');
require("class/Formation.php");
require("class/Etablissement.php");
require("utils/methodes.php");
?>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <link href="style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
  integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
  crossorigin=""/>

  <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
  integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
  crossorigin=""></script>
    <title>Post Bac</title>
  </head>

  <body>
    <nav>
      <ul>
        <li><a href="index.html" class="nav navlogo"><img src="files/logo.png" class="logo" alt="Logo">
           PostBac
        </a>
      </li>
        <li><a href="formations.php" class="nav formation">Formations</a></li>
        <li><a href="region.php" class="nav region">Régions</a></li>
        <li><a href="bestFormation.php" class="nav information">Les + visités</a></li>
        <li><a href="informations.html" class="nav information">Informations</a></li>

      </ul>
    </nav>
    <div class="contenu">
      <br>
      <div class="container">
        <div class="test">
        <?php

        if (isset($_GET)) {
            $code = $_GET["id"];

            //Compter le nombre de visiteur

            //On forme l'array
            $array = createAllCount("utils/counts/count_etab.txt");

            # print_r($array);
            // On ajoute 1 visiteur
            addNewElement($array, $code, "utils/counts/count_etab.txt");

            // On affiche la valeur qui est à jour
            $array = createAllCount("utils/counts/count_etab.txt");

            $url_getuai = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-etablissements-enseignement-superieur&facet=uai&facet=type_d_etablissement&facet=com_nom&facet=dep_nom&facet=aca_nom&facet=reg_nom&facet=pays_etranger_acheminement";
            $url_getuai .= "&refine.uai=".$code;
            $res = API::getInformations($url_getuai);

            //On prend les champs

            $tableau = array();
            foreach($res["records"][0]["fields"] as $key=>$value) {
              $tableau[$key] = $value;
            }

            // print_r($res);

            $etab = new Etablissement($code, $tableau["uo_lib"], $tableau["coordonnees"][0], $tableau["coordonnees"][1], $tableau["url"],  $tableau["uucr_nom"], $tableau["element_wikidata"], $tableau["adresse_uai"], $tableau["type_d_etablissement"]);
            ?>
        <div class="left">
        <h1><?php echo $etab->getName() ?></h1>
        <br>

        <p>Nombre de visite : <?php echo $array[$code] ?></p>
        <?php
            $etab->toString(); ?>
        </div>
            <div id="mapid">
              <script>
                var mymap = L.map('mapid').setView([48.85661, 2.35222], 5);
                L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
                attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                maxZoom: 16,
                id: 'mapbox/streets-v11',
                accessToken: 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw'
                }).addTo(mymap);

                var marker = L.marker([<?php echo $etab->getCoord()[0]; ?>, <?php echo $etab->getCoord()[1]; ?>]).addTo(mymap);
                marker.bindPopup("<b><?php echo $etab->getName(); ?></b><br><p><?php echo $etab->getVille(); ?></p>").openPopup();
              </script>
            </div>
        <?php }
        ?>
      </div>
    </div>
    </div>
    <footer>
      <p>Le repo : <a href="https://bitbucket.org/Qilowa/opendata/src/master/">https://bitbucket.org/Qilowa/opendata/src/master/</a></p>
    </footer>
  </body>

</html>
