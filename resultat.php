<!DOCTYPE html>
<?php
require("class/API.php");
require("class/Etablissement.php");
require("class/Formation.php");
 ?>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <link href="style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
  integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
  crossorigin=""/>

  <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
  integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
  crossorigin=""></script>
    <title>PostBac</title>
  </head>

  <body>
    <nav>
      <ul>
        <li><a href="index.html" class="nav navlogo"><img src="files/logo.png" class="logo" alt="Logo">
           PostBac
        </a>
      </li>
        <li><a href="formations.php" class="nav formation">Formations</a></li>
        <li><a href="region.php" class="nav region">Régions</a></li>
        <li><a href="bestFormation.php" class="nav information">Les + visités</a></li>
        <li><a href="informations.html" class="nav information">Informations</a></li>
      </ul>
    </nav>
    <div class="contenu">
      <br>
      <div class="container">
      <h1>Résultats</h1>


    <?php

      if (isset($_POST)) {
        if (isset($_POST["recherche"])) {
          $ajout = $_POST["recherche"];
          $url = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=15&q=".$ajout;
          $url .= "&refine.rentree_lib=2017-18";
          $results = API::getInformations($url);

          // print_r(array_keys($results));
          // print_r(array_keys($results[4]));
          // echo "\n";


          if ($results["nhits"] == 0) {
            // Pas de résultat avec les critères demandés
            echo "<p>Pas de résultats. Veuillez réessayer.</p>";
          } else {

         ?>

         <div class="test">
           <div class="left">
           <h2>Carte</h2>
         <div id="mapid">
           <script>
             var mymap = L.map('mapid').setView([48.85661, 2.35222], 16);
             L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 16,
            id: 'mapbox/streets-v11',
            accessToken: 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw'
            }).addTo(mymap);


          <?php
               foreach ( $results["records"] as $key => $value) {

                  $code = $value["fields"]["etablissement"];

                  $nom = $value["fields"]["etablissement_lib"];

                  $url_getuai = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-etablissements-enseignement-superieur&facet=uai&facet=type_d_etablissement&facet=com_nom&facet=dep_nom&facet=aca_nom&facet=reg_nom&facet=pays_etranger_acheminement";

                  $url_getuai .= "&refine.uai=".$code;

                  $res = API::getInformations($url_getuai);

                  $coord1 = $res["records"][0]["fields"]["coordonnees"][0];
                  $coord2 = $res["records"][0]["fields"]["coordonnees"][1];

                  $etab_url = $res["records"][0]["fields"]["url"];

                  $uucr_nom = $res["records"][0]["fields"]["uucr_nom"];


                  if (isset($coord1) && isset($coord2)) {
                     $etab = new Etablissement($code, $nom, $coord1, $coord2, $etab_url,  $uucr_nom);

                  ?>
                     var marker = L.marker([<?php echo $etab->getCoord()[0]; ?>, <?php echo $etab->getCoord()[1]; ?>]).addTo(mymap);
                     marker.bindPopup("<b><?php echo $etab->getName(); ?></b><br><p>Diplome : <?php echo $value["fields"]["typ_diplome_lib"]; ?></p><p><?php echo $etab->getVille(); ?></p>").openPopup();


                <?php

                   }
               }
                  ?>

                  </script>
                    </div>
                  </div>
                  <br>
                  <br>
                  <br>
               <div class="right">
               <h2>Tableau récapitulatif</h2>
               <h3>Nombre de résultats : <?php echo sizeof($results["records"]) ?></h3>
               <div class="result_tab">
                 <table>
                     <tr>
                       <th>Nom Etablissement</th>
                       <th>Secteur</th>
                       <th>Niveau</th>
                       <th>Type de diplome</th>
                       <th>Ville</th>
                       <th>Effectif</th>
                       <th>Plus</th>
                     </tr>
                   <?php
                  foreach ( $results["records"] as $key => $value) {
                       $libelle = $value["fields"]["libelle_intitule_1"];
                       $dip = $value["fields"]["diplome_rgp"];
                       $loc = $value["fields"]["localisation_etab"];
                       $ville = $value["fields"]["com_ins_lib"];
                       $wiki = $value["fields"]["element_wikidata"];
                       $forma = new Formation($value["fields"]["etablissement_lib"], $value["fields"]["sect_disciplinaire_lib"], $value["fields"]["niveau_lib"], $value["fields"]["typ_diplome_lib"], $value["fields"]["effectif"], $value["fields"]["diplom"]);
                           echo "<tr>\n";
                           echo "<td><a href='getEtablissement.php?id=".$code."' target='_blank'>".$forma->getNomEtab()."</a></td>\n";
                           echo "<td>".$forma->getSecteur()."</td>\n";
                           echo "<td>".$forma->getNiveau()."</td>\n";
                           echo "<td>".$forma->getType()."</td>\n";
                           echo "<td>".$ville."</td>\n";
                           echo "<td>".$forma->getEffectif()."</td>\n"; ?>
                           <td>
                             <form action="getFormation.php?sect=<?php echo $forma->getSecteur() ?>&niv=<?php echo $forma->getNiveau(); ?>&ville=<?php echo $ville ?>&effectif=<?php echo $forma->getEffectif() ?>&etab=<?php echo $forma->getNomEtab(); ?>&lib=<?php echo $libelle; ?>&diplome=<?php echo $dip; ?>&loc=<?php echo $loc; ?>&wiki=<?php echo $wiki; ?>&site=<?php echo $etab->getUrl(); ?>">
                               <input type="submit" value="Plus d'info" class="infos">
                             </form>
                           </td>

                           <?php
                           echo "</tr>\n";

                        }

                   ?>
                   </table>
                 </div>
             </div>
             </div>

      <?php }
    }
  }
       ?>

    </div>
  </div>
  <div>
  <footer>
    <p>Le repo : <a href="https://bitbucket.org/Qilowa/opendata/src/master/">https://bitbucket.org/Qilowa/opendata/src/master/</a></p>
  </footer>
 </div>
   </body>
 </html>
